# ClamshellPC
ノートPCをクラムシェル（閉じたまま）使うためのノウハウ

HSRの背中に積んだPCのディスプレイを開けたまま運用するのは危険です。

<img src="images/image1.png" width="512">

## 実行環境
Ubuntu 20.04

## 蓋を閉じてもスリープにしない方法
```
sudo vi /etc/systemd/logind.conf
```
```
# HandleLidSwitch=sudpend
HandleLidSwitch=ignore
```
保存して、下記のコマンドでサービスを再起動する
```
systemctl restart systemd-logind.service
```

## 放置してもスリープ状態にしない方法
キーボード等が一定時間操作されなかった場合、省電力のためサスペンドされてしまいまう場合があります。

リモート接続だと端末を操作しないため、この機能によってスリープ状態となります。

```
sudo vi /usr/share/gdm/dconf/99-local-settings
```
ファイル内に以下の設定を追記します。
```
[org / gnome / settings-daemon / plugins / power]
sleep-inactive-ac-timeout = 0
sleep-inactive-battery-timeout = 0
```
ファイルを保存すれば設定は完了です。


## バッテリーの残量をモニターする
クラムシェルモードにしてリモート接続で運用するとバッテーリーの残量が心配です。
バッテリーの充電状況や劣化の具合は upowerコマンドで確認できます。

###  電源デバイスの確認
```
$ upower -e
/org/freedesktop/UPower/devices/line_power_AC
/org/freedesktop/UPower/devices/battery_BAT0
/org/freedesktop/UPower/devices/line_power_ucsi_source_psy_USBC000o001
/org/freedesktop/UPower/devices/line_power_ucsi_source_psy_USBC000o002
/org/freedesktop/UPower/devices/DisplayDevice
```

### 指定した電源デバイスの状態表示
```
$ upower -i /org/freedesktop/UPower/devices/battery_BAT0
  native-path:          BAT0
  vendor:               SMP
  model:                01YU911
  serial:               1549
  power supply:         yes
  updated:              2023年05月21日 09時06分32秒 (85 seconds ago)
  has history:          yes
  has statistics:       yes
  battery
    present:             yes
    rechargeable:        yes
    state:               fully-charged
    warning-level:       none
    energy:              70.07 Wh
    energy-empty:        0 Wh
    energy-full:         70.07 Wh
    energy-full-design:  80.4 Wh
    energy-rate:         0 W
    voltage:             16.928 V
    percentage:          100%
    capacity:            87.1517%
    technology:          lithium-polymer
    icon-name:          'battery-full-charged-symbolic'
```

### バッテリーの残量を求める
```
$ upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -e "percentage" -e "time to empty"
   percentage:          100%
```

```
$ upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -e "percentage" |awk '{print $2}'
100%
```

### リモートマシンのバッテリーの残量を求める
rsh を使って、他のマシンのバッテリーの残量を求めます。

```
$ rsh hsrb.local upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -e "percentage" |awk '{print $2}'
100%
```
パスワードなしでrshを実行する方法は各自で調べてください。

## sshpassを使ってリモートアクセスする
### sshpassのインストール
```
sudo apt-get install sshpass
```
### コマンドラインから使う
ssh で接続する
```
sshpass -p パスワード ssh ユーザID@マシン名 
```

rsh でコマンドを実行する
```
sshpass -p パスワード rsh ユーザID@マシン名  コマンド
```

### Pythonプログラムで使う
```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import subprocess

res = subprocess.run(
    "sshpass -p パスワード rsh ユーザ名@マシン名 upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -e percentage |awk '{print $2}'",
    capture_output=True,
    text=True,
    shell=True
)

if res.returncode==0: # リターンコード
    print(res.stdout) # 標準出力に結果が入る
```


## リモートマシンを接続して開発する
### ローカルマシン mypc.local （デバッグ用PC）の設定
- Ubuntu 20.04
- ROS noetic
- HSR開発環境
```
roboworks@mypc:~$ hsrb_mode
<hsrb>~$
```

### ローカルマシン pc1.local （背中のPC）の設定
- Ubuntu 20.04
- ROS noetic
- HSR開発環境
```
roboworks@pc1:~$ hsrb_mode
<hsrb>~$
```


### SSH（Secure SHell）
mypcのターミナルからpc1（HSRの背中のPC）にSSHで接続する
```
roboworks@mypc:~$ ssh -X pc1.local -t tmux

roboworks@pc1:~$ 
```

### リモートシェル(rsh)






# 開発PCからコントローラでHSRを操作する
https://gitlab.com/okadalaboratory/hsr-development/controller


# 実践
リモートでHSRのROSノードを起動する

https://gitlab.com/okadalaboratory/remote-launch